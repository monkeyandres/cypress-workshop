import { withFormik } from 'formik'
import axios from 'axios'

const enhance = withFormik({
  mapPropsToValues: () => ({
    firstPet: '',
    favoriteFood: '',
    goikoGrill: '',
    redradixEmployee: ''
  }),
  handleSubmit: (values, { setSubmitting, props }) => {
    axios
      .post('https://someapi.com/food', values)
      .then(() => {
        setSubmitting(false)
        props.history.push('/success')
      })
      .catch(() => console.error('error calling api'))
  }
})

export default enhance
