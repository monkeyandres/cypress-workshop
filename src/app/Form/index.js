import Form from './component'
import enhance from './enhance'

export default enhance(Form)
