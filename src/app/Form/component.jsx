import React from 'react'

const Form = props => (
  <React.Fragment>
    <h2>Example Form</h2>

    <form onSubmit={props.handleSubmit}>
      <p>
        <label htmlFor="firstPet">Name of your first pet</label>
        <input
          type="text"
          name="firstPet"
          id="firstPet"
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          value={props.values.firstPet}
        />
      </p>

      <p>
        <label htmlFor="favoriteFood">What's your favorite food?</label>
        <input
          type="text"
          name="favoriteFood"
          id="favoriteFood"
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          value={props.values.favoriteFood}
        />
      </p>

      <p>
        <label htmlFor="goikoGrill">How ofter do you go to Goiko Grill?</label>
        <select
          name="goikoGrill"
          id="goikoGrill"
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          value={props.values.goikoGrill}
        >
          <option value="" />
          <option value="never">Never</option>
          <option value="sometimes">Sometimes</option>
          <option value="everyday">Everyday</option>
        </select>
      </p>

      <p>
        <label htmlFor="redradixEmployee">Do you work in Redradix?</label>
        <input
          type="checkbox"
          name="redradixEmployee"
          id="redradixEmployee"
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          value={props.values.redradixEmployee}
        />
      </p>

      <p>
        <button disabled={props.isSubmitting}>Submit</button>
      </p>
    </form>
  </React.Fragment>
)

export default Form
