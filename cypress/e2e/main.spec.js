/* globals cy, before, after */
/// <reference types="Cypress" />

describe('Main Flow', () => {
  before(() => {
    cy.visit('/')
    cy.url().should('include', 'http://localhost:3000/')
  })

  it('go to form', () => {
    cy.get('a')
      .contains('Form')
      .click()

    cy.url().should('include', '/form')
  })

  describe('fill the form fields', () => {
    describe('fill the pet name', () => {
      const inputName = ['firstPet', 'favoriteFood']

      inputName.forEach(name => {
        it(`fill ${name} input`, () => {
          cy.fixture('formData').then(data => {
            cy.get(`input[name=${name}]`).type(data[name])
          })
        })
      })
    })

    it('fill the select', () => {
      cy.get('select[name=goikoGrill]').select('everyday')
    })

    it('fill the checkbox', () => {
      cy.get('input[name=redradixEmployee]').check()
    })
  })

  describe('submit the form', () => {
    before(() => {
      cy.server()

      cy.route('POST', 'https://someapi.com/food', { status: 'ok' })
    })

    it('submits the form', () => {
      cy.get('button')
        .contains('Submit')
        .click()

      cy.url().should('include', '/success')
    })
  })
})
